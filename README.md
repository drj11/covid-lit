# covid-lit

COVID literature

## Transmission

Closed environments facilitate secondary transmission of coronavirus disease 2019 (COVID-19)
https://www.medrxiv.org/content/10.1101/2020.02.28.20029272v2

## Tracing

[ATI20200504] Ada Lovelace
https://www.adalovelaceinstitute.org/our-work/covid-19/provisos-for-a-contact-tracing-app-4-may-2020/

[COLDICUTT20200323] Open Letter: Contact Tracking and NHSX
https://medium.com/@rachelcoldicutt/open-letter-contract-tracking-and-nhsx-e503325b2703

[NCSC2020] https://www.ncsc.gov.uk/blog-post/security-behind-nhs-contact-tracing-app

[NCSC20200503] https://www.ncsc.gov.uk/files/NHS-app-security-paper%20V0.1.pdf

[NHSX20200424]
Digital contact tracing: protecting the NHS and saving lives
NHSX
https://www.nhsx.nhs.uk/blogs/digital-contact-tracing-protecting-nhs-and-saving-lives/]


## Reviews

### ATI20200504

This blog is clearly skeptical that the NHSX and the UK government can publish a contcat tracing app that is transparent enough for the public to safely use it.